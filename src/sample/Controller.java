package sample;

import javafx.animation.AnimationTimer;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private Canvas canvas;

    private GraphicsContext context;
    private Box box;

    @FXML
    private ColorPicker colorPicker;

    @FXML
    private Slider slider;

    private List<Ball> balls = new ArrayList<>();
    private double canvasWidth;
    private double canvasHeight;
    private Parent root;
    private Color color;
    private float radius;
    private Random random = new Random();
    private float speed;
    private float angle;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        canvasWidth = canvas.getWidth();
        canvasHeight = canvas.getHeight();
        context = canvas.getGraphicsContext2D();
        box = new Box(0, 0, canvasWidth, canvasHeight, Color.WHITE);

        colorPicker.setOnAction(event -> {
            color = colorPicker.getValue();
        });

        slider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                radius = (float) slider.getValue();
            }
        });
        this.setup();

        canvas.setOnMouseClicked(mouseEvent -> {
            speed = 2 + random.nextFloat() * 8;
            angle = random.nextFloat() * 360;
            balls.add(new Ball((float) mouseEvent.getX(), (float) mouseEvent.getY(), radius, speed, angle, color));
            System.out.println(speed + " " + angle);
            this.draw();
        });
    }

    private void draw() {
        for (Ball ball : this.balls) {
            if (ball != null) {
                ball.draw(context);
            }
        }
    }

    private void setup() {
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long l) {
                update();
                redraw();
            }
        };
        timer.start();
    }

    private void update() {
        for (Ball ball : this.balls) {
            if (ball != null) {
                ball.update(box);
            }
        }
    }

    private void redraw() {
        box.draw(context);
        for (Ball ball : this.balls) {
            if (ball != null) {

                ball.draw(context);
            }
        }
    }

    public void setRoot(Parent root) {
        this.root = root;
    }
}
